#gitlab-runner register  --url https://gitlab.com  --token glrt-Q6_23va7n9Ni1QfCwy7K
#gitlab-runner run

VOLUME_NAME=gitlab-runner-terraform
RUNNER_CONTAINER_NAME=gitlab-runner-terraform
SECRET_RUNNER=glrt-xkC7xhzAPiy793iHNf9R
source secret.env

function createVolume(){
    docker volume create "${VOLUME_NAME}"
}


function register(){
    docker run --rm \
        -v "${VOLUME_NAME}":/etc/gitlab-runner \
        -v /var/run/docker.sock:/var/run/docker.sock \
        gitlab/gitlab-runner:alpine-v16.5.0 register \
            --non-interactive \
            --url "https://gitlab.com/" \
            --token "${SECRET_RUNNER}" \
            --executor "docker"\
            --docker-image alpine:latest \
            --description "Runner for terraform pipeline"
}

function runnerRun(){
    docker container stop "${RUNNER_CONTAINER_NAME}"
    docker container rm "${RUNNER_CONTAINER_NAME}"
    docker volume rm "${VOLUME_NAME}"
    docker run -d --name "${RUNNER_CONTAINER_NAME}" --restart always \
        --env TZ=IT\
        -v /var/run/docker.sock:/var/run/docker.sock \
        -v "${VOLUME_NAME}":/etc/gitlab-runner \
        gitlab/gitlab-runner:alpine-v16.5.0
}

createVolume
runnerRun
register
